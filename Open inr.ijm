file=getArgument();
header = File.openAsRawString(file,256);

index1 = indexOf(header, "XDIM=",0);
index2 = indexOf(header,"\n",index1);
xdim = substring(header,index1+5,index2);

index1 = indexOf(header, "YDIM=",0);
index2 = indexOf(header,"\n",index1);
ydim = substring(header,index1+5,index2);

index1 = indexOf(header, "ZDIM=",0);
index2 = indexOf(header,"\n",index1);
zdim = substring(header,index1+5,index2);


run("Raw...", "open="+file+" image=[16-bit Unsigned] width="+xdim+" height="+ydim+" offset=256 number="+zdim+" little-endian");

