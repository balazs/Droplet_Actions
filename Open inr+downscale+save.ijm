path=getArgument();
filename = File.getName(path);
folder = File.getParent(path);
newFile = substring(filename,0,lastIndexOf(filename,".")) + "_small.h5";
newPath = folder + File.separator + newFile;
header = File.openAsRawString(path,256);

index1 = indexOf(header, "XDIM=",0);
index2 = indexOf(header,"\n",index1);
xdim = substring(header,index1+5,index2);

index1 = indexOf(header, "YDIM=",0);
index2 = indexOf(header,"\n",index1);
ydim = substring(header,index1+5,index2);

index1 = indexOf(header, "ZDIM=",0);
index2 = indexOf(header,"\n",index1);
zdim = substring(header,index1+5,index2);


run("Raw...", "open="+path+" image=[16-bit Unsigned] width="+xdim+" height="+ydim+" offset=256 number="+zdim+" little-endian");

run("Scale...", "x=- y=- z=- width="+round(xdim/2)+" height="+round(ydim/2)+" depth="+round(zdim/2)+" interpolation=None process create");

run("Scriptable save HDF5 (new or replace)...", "save="+newPath+" dsetnametemplate=/Data formattime=%d formatchannel=%d compressionlevel=0");

close();
close();