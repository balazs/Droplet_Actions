file=getArgument();
header = File.openAsRawString(file,256);

index1 = indexOf(header, "XDIM=",0);
index2 = indexOf(header,"\n",index1);
xdim = substring(header,index1+5,index2);

index1 = indexOf(header, "YDIM=",0);
index2 = indexOf(header,"\n",index1);
ydim = substring(header,index1+5,index2);

index1 = indexOf(header, "ZDIM=",0);
index2 = indexOf(header,"\n",index1);
zdim = substring(header,index1+5,index2);


run("Raw...", "open="+file+" image=[16-bit Unsigned] width="+xdim+" height="+ydim+" offset=256 number="+zdim+" little-endian");

run("segmentMatlab");
setMinAndMax(0, 255);
run("RGB Color");

filename = substring(file,lastIndexOf(file,"\\")+1,lengthOf(file));
print(filename);

filename = File.getName(file);
print(filename);

run("3D Viewer");
call("ij3d.ImageJ3DViewer.setCoordinateSystem", "false");
call("ij3d.ImageJ3DViewer.add", filename, "None", filename, "0", "true", "true", "true", "2", "0");
