# Droplet Actions

## Description
This project contains a collection of macros to be used with the Droplet plugin for Fiji/ImageJ.

## Installation
1. Download the droplet plugin from the ImageJ wiki website: [http://imagejdocu.tudor.lu/doku.php?id=plugin:utilities:droplet:start](http://imagejdocu.tudor.lu/doku.php?id=plugin:utilities:droplet:start).
2. Save it in the "Fiji.app/plugins" folder
3. In the "plugins" folder `git clone https://git.embl.de/balazs/Droplet_Actions.git`  
    OR  
    [Download repository in zip format](https://git.embl.de/balazs/Droplet_Actions/repository/archive.zip?ref=master) and unzip to "Fiji.app/plugins"
5. Rename to "Droplet Actions"


## Usage
1. Run Fiji/ImageJ
2. Plugins > Droplet
3. Select desired action from dropdown list
4. Drop files/folders

> Some droplets might require additional plugins, such as HDF.