path=getArgument();

folder = File.getParent(path);
filename = File.getName(path);
//print(folder);
segFile = "seg_" + substring(filename,0,lastIndexOf(filename,".")) + ".tif";
//print(newname);
logFile = "segmentation.txt";
logPath = folder + File.separator + logFile;

run("Scriptable load HDF5...", "load="+path+" datasetnames=/Data nframes=1 nchannels=1");
rename("orig");
getDimensions(width, height, channels, slices, frames);

run("Scale...", "x=- y=- z=- width="+round(width/2)+" height="+round(height/2)+" depth="+round(slices/2)+" interpolation=Bilinear average process create");
rename("small");
run("Duplicate...", "duplicate");
run("Gaussian Blur 3D...", "x=2.5 y=2.5 z=2.5");
rename("small_gauss_25");

run("3D Spot Segmentation", "seeds_threshold=2000 local_background=0 local_diff=0 radius_0=8 radius_1=10 radius_2=12 weigth=0.50 radius_max=10 sd_value=1 local_threshold=[Local Mean] seg_spot=Classical watershed volume_min=200 volume_max=1000000 seeds=Automatic spots=small_gauss_25 radius_for_seeds=11 output=[Label Image]");
//run("Duplicate...", "duplicate");
//rename("seg");

//Get content from Log Window + Print to file
selectWindow("Log");
content = getInfo();
i1 = lastIndexOf(content, "Create label image with ") + 24;
i2 = lastIndexOf(content, "objects");
print(i1,i2);
//print(f,content);
File.append(filename + "\t" + (substring(content,i1,i2)),logPath);

// display result
run("glasbey inverted");
setMinAndMax(0, 255);

selectWindow("small");
resetMinAndMax();
run("Merge Channels...", "c1=small c2=seg create keep");
rename("merged");
run("Save", "save="+folder + File.separator + segFile);

//close intermediate windows
w = newArray("orig", "small", "small_gauss_25", "seg");
for (i=0; i<w.length; i++) {
	selectWindow(w[i]);
	close();
}

