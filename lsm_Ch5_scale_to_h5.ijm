path=getArgument();

run("Bio-Formats", "open="+path+" color_mode=Default specify_range view=[Standard ImageJ] stack_order=Default c_begin=5 c_end=5 c_step=1 z_begin=1 z_end=100 z_step=1");

folder = File.getParent(path);
filename = File.getName(path);
//print(folder);
newname = substring(filename,0,lastIndexOf(filename,".")) + ".h5";
//print(newname);
newfolder = folder + File.separator + "scaled";
if (!File.exists(newfolder)) {
	File.makeDirectory(newfolder);
}
newpath = newfolder + File.separator + newname;
//print(newpath);

getDimensions(width, height, channels, slices, frames);
getVoxelSize(pw, ph, pd, unit);
zScale  = (pd / 2) / pw;
run("Scale...", "x=- y=- z=- width="+(width/2)+" height="+(height/2)+" depth="+round(slices*zScale)+" interpolation=Bilinear average process create");
run("Scriptable save HDF5 (new or replace)...", "save="+newpath+" dsetnametemplate=/Data formattime=%d formatchannel=%d compressionlevel=0");
close();
close();

