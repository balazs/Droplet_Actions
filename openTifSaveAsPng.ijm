path=getArgument();
open(path);
folder = File.getParent(path);
filename = File.getName(path);
//print(folder);
newname = substring(filename,0,lastIndexOf(filename,".")) + ".png";
//print(newname);
newfolder = folder + File.separator + "png";
if (!File.exists(newfolder)) {
	File.makeDirectory(newfolder);
}
newpath = newfolder + File.separator + newname;
//print(newpath);
run("RGB Color");
saveAs("PNG", newpath);
close();

